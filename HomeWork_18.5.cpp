﻿#include <iostream>

using namespace std;

class CStack
{
private:
	int* bottom_;
	int* top_;
	int size_;
public:

	CStack(int n = 20)
	{
		bottom_ = new int[n];
		top_ = bottom_;
		size_ = n;
	}
	void push(int c)
	{
		if (size_ < 20)
		{
			*top_ = c;
			top_++;
		}
	}
	void pop()
	{
		if (size_ > 0)
			--size_;
		else
		{
			cout << "Стек пуст";
		}
	}
	~CStack()
	{ // stacks when exiting functions
		delete[] bottom_;
	}
	void show()
	{
		if (size_ > 0)
		{
			for (int i = size_ - 1; i >= 0; --i)
				cout << bottom_[i] << " ";
			cout << endl;
		}
		else
			cout << "Стек пуст" << endl;
	}
};

int main()
{
	setlocale(LC_ALL, "Rus");
	int ct = 0;
	int a;
	int el;
	cout << "Создадим массив и погрузим его в стек\n";
	cout << "Для этого введите желаемый размер массива: ";
	cin >> a;
	CStack s(a);
	cout << "Массив на " << a << " элемента(ов) создан, давайте его наполним..\n";
	cout << "Для этого введите " << a << " элемента(ов)\n";
	while (ct++ < a)
	{
		cin >> el;
		s.push(el);
	}
	s.show(); cout << "\n";
	cout << "Удалим 2 элемента\n";
	s.pop();
	s.pop();
	cout << "Теперь в нашем стеке содержатся:\n";
	s.show(); cout << "\n";
}